#!/bin/bash
read -p 'KEYNAME: ' KEYNAME
#read -p 'HOST: ' MACHINE_IP
MACHINE_IP=$(./get_EC2_IP.sh)
#read -p 'USER: ' REMUSER
echo $REMUSER
#copy provisioning script to instance
scp  -i $KEYNAME provision.sh ubuntu@$MACHINE_IP:/home/ubuntu
#copy openvpn file to instance
scp  -i $KEYNAME /home/pen/Downloads/starting_point_dry25.ovpn ubuntu@$MACHINE_IP:/home/ubuntu
#scp  -i /home/pen/Downloads/tf-ssh.pem ./provision.sh ubuntu@$MACHINE_IP:/home/ubuntu

#ssh -i /home/pen/Downloads/tf-ssh.pem ubuntu@$MACHINE_IP
ssh -i $KEYNAME ubuntu@$MACHINE_IP
