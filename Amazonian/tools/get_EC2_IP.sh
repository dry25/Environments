#!/bin/bash

#aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId, PublicIpAddress]' --output table

##output to text file 
aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId, PublicIpAddress]' >ip.txt
##looks for valid IP addresses using regex and stores in file
grep -E "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" ip.txt> ipOnly.txt
##removes blank data and quotation marks and displays IP 
cat ipOnly.txt | tr  -d '[:blank:]' | tr  -d '"'
##deletes all files
rm ip*