resource "aws_internet_gateway" "pptest-env-gw" {
  vpc_id = "${aws_vpc.ptest-env.id}"

tags = {
    Name = "ptest-env-gw"
  }  
}