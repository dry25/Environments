resource "aws_vpc" "ptest-env" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
tags ={
    Name = "ptest-env"
  }
}
resource "aws_eip" "ip-ptest-env" {
  instance = "${aws_instance.ptest-ec2-instance.id}"
  vpc      = true
}
