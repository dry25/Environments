# Environments



## General Purpose

The goal of this project is to document terraform configuration of test machines in various cloud environments. 


## Usage
There will be a different directory for each cloud Provider which will contain the relevant terraform code, as well as a folder containing tools for interacting with the machine. 

## Requirements 
Please note that you will need the following configurations to use these templates:

- Terraform installed on your machine
- Relevant cloud provider cli installed with credentials configured


## Roadmap
The plan is as follows:

- Have terraform templates for AWS, AZURE, GCP and Digital ocean environments
- Have a list of necessary tools needed on each machine
- Have a script that installs all necessary packages on the machine. 
